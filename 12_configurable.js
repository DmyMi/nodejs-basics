"use strict";

const connect = require('connect');
const app = connect();

app.use(setup({greet: "Hello", who: "Vasya"}));
app.listen(3000);

function setup(options) {
    const greeting = options.greet;
    const name = options.who;

    return (req, res, next) => {
        res.setHeader('Content-Type', 'text/plain');
        res.end(`${greeting} ${name}`);
    }
}