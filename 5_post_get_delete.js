const http = require('http');
const url = require('url');
const items = [];

const server = http.createServer(function(req, res){
    switch (req.method) {
        case 'POST':
            let item = '';
            req.setEncoding('utf8');
            req.on('data', function(chunk){
                item += chunk;
            });
            req.on('end', function(){
                items.push(item);
                res.end('OK\n');
            });
            break;
        case 'GET':
            items.forEach(function(item, i){
                res.write(i + ') ' + item + '\n');
            });
            res.end();
            break;
        case 'DELETE':
            console.log('url parameters are: ', url.parse('http://localhost:3000/1?key=value'));
            let path = url.parse(req.url).pathname;
            let i = parseInt(path.slice(1), 10);
            if (isNaN(i)) {
                res.statusCode = 400;
                res.end('Invalid item id');
            } else if (!items[i]) {
                res.statusCode = 404;
                res.end('Item not found');
            } else {
                items.splice(i, 1);
                res.end('OK\n');
            }
            break;
    }
});

server.listen(3000);

// curl localhost:3000 -i -d 'test text super text much text test test'
// curl 'http://localhost:3000/1?key=value' -i -XDELETE