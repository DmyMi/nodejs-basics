"use strict";

const connect = require('connect');
const path = require('path');
const serveStatic = require('serve-static'); //as of ~2014 static is separate module
const app = connect()
    .use(serveStatic(path.join(__dirname, 'public')))
    .listen(3000);