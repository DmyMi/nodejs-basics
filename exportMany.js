const dollar = 26.0;

function roundTwoDecimals(amount) {
    return Math.round(amount * 100) / 100;
}

exports.UAToUS = function(ua) {
    return roundTwoDecimals(ua / dollar);
};

exports.USToUA = function(us) {
    return roundTwoDecimals(us * dollar);
};