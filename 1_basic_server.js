"use strict";

const http = require('http');
const server = http.createServer(function(req, res){
    res.write('Hello World\r\n');
    res.end(); // or you can just write res.end('Hello World');
});

server.listen(3000);

// curl localhost:3000 -i