"use strict";

const connect = require('connect');

function badMiddleware(req, res, next) {
    next(new Error('Bad middleware makes error'));
}

function errorHandler(err, req, res, next) {
    console.error("Reason: ", err);
    res.statusCode = 500;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({code: 500, message: "Server Error"}));
}

connect()
    .use(badMiddleware)
    .use(errorHandler)
    .listen(3000);