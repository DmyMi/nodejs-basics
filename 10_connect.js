"use strict";

const connect = require('connect');
const app = connect();

app.use(logger);
app.use('/admin', admin);
app.use('/user', user);
app.use(hello);
app.listen(3000);

function logger(req, res, next) {
    console.log('%s %s', req.method, req.url);
    next();
}

function hello(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('hello world');
}

function admin(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.end('Sorry this is accessible only by admin!');
}

function user(req, res) {
    let name = '';
    switch (req.url) {
        case '/':
            res.end('try /vasya or /petya');
            break;
        case '/vasya':
            name = "Vasiliy";
            break;
        case '/petya':
            name = "Petya the Virus";
            break;
    }
    res.setHeader('Content-Type', 'text/plain');
    res.end(`hello ${name}`);
}