"use strict";

const connect = require('connect');
const bodyParser = require('body-parser'); //as of ~2014 body parser is separate module
const app = connect()
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: false }))
    .use(function(req, res){
        // .. do stuff to register the user ..
        res.end('Registered new user: ' + req.body.username);
    })
    .listen(3000);

// JSON data
// curl -d '{"username":"Vasya"}' -H "Content-Type: application/json" http://localhost:3000/

//Form data
// curl -d username=Petya http://localhost:3000/