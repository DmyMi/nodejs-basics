const exchange = require('./exportMany');
const Currency = require('./exportOne');

console.log("200 UAH is: ", exchange.UAToUS(200), '$');
console.log("200 US is: ", exchange.USToUA(200), 'uah');

const currency = new Currency(26.0);
console.log("300 UAH is: ", currency.UAToUS(300), '$');
console.log("300 US is: ", currency.USToUA(300), 'uah');