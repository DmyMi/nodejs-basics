'use strict';

class Exchange {
    constructor(rate) {
        this.rate = rate;
    }

    roundTwoDecimals(amount) {
        return Math.round(amount * 100) / 100;
    }

    UAToUS(ua) {
        return this.roundTwoDecimals(ua / this.rate);
    };

    USToUA(us) {
        return this.roundTwoDecimals(us * this.rate);
    };
}

module.exports = Exchange;