"use strict";

const connect = require('connect');
const cookieParser = require('cookie-parser'); //as of 2014 cookie parser is separate module
const app = connect()
    .use(cookieParser())
    .use('/cookie', function(req, res) {
        res.setHeader('Set-Cookie', 'foo=bar');
        res.end();
    })
    .use(function(req, res){
        console.log(req.cookies);
        res.end('hello\n');
    }).listen(3000);

// curl http://localhost:3000/

// curl http://localhost:3000/ -H "Cookie: foo=bar, bar=baz"

// JSON cookie - prefix j:
// curl http://localhost:3000/ -H 'Cookie: name=j:{"foo":"bar"}'

// curl http://localhost:3000/cookie --head