"use strict";

const http = require('http');
const server = http.createServer(function(req, res){
    /*
    By default, the data events provide Buffer objects, which are Node’s version of byte
    arrays. In the case of text items, you don’t need binary data, so setting the
    stream encoding to utf8 is ideal; the data events will instead emit strings.
    Next line demonstrates this
     */
    req.setEncoding('utf8');
    req.on('data', function(chunk){
        console.log('parsed', chunk);
    });
    req.on('end', function(){
        console.log('done parsing');
        res.end()
    });
});

server.listen(3000);
// curl localhost:3000 -i -d 'test text'