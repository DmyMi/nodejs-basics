"use strict";

const express = require('express');
const path = require('path');
const app = express();

app.set('view engine', 'pug')
app.use("/static", express.static(path.join(__dirname, "public")));

const sampleUser = {username: 'Vasya'};
app
.get('/', function (req, res) {
    res.render('index', 
        { title: 'Hey', message: 'Hello there!', user: sampleUser, langs: ['kotlin', 'javascript']}
    );
})
.listen(3000);