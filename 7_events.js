const EventEmitter = require('events').EventEmitter;
const http = require('http');

const dummyEvent = new EventEmitter();

dummyEvent.on('start', function(data) {
    console.log("Something is received, it looks like: ", data);
});

dummyEvent.on('end', function () {
    console.log("\nResponse sent");
});

const server = http.createServer(function(req, res){
    dummyEvent.emit('start', req);
    res.end('Hello World');
    dummyEvent.emit('end');
});

server.listen(3000);

// curl localhost:3000 -i