"use strict";

const connectRoute = require('connect-route');
const connect = require('connect')
const app = connect();
const router = require('./connect_router');

app.use(connectRoute(router));
app.listen(3000);

// curl -X POST http://localhost:3000/home