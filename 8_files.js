const http = require('http');
const parse = require('url').parse;
const join = require('path').join;
const fs = require('fs');
const root = __dirname; // magic variable, points to current file's location

const server = http.createServer(function(req, res){
    const url = parse(req.url);
    const path = join(root, url.pathname);
    const stream = fs.createReadStream(path);
    stream.on('data', function(chunk){
        res.write(chunk);
    });
    stream.on('end', function(){
        res.end();
    });
});
server.listen(3000);